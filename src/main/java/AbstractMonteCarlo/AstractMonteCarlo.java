package AbstractMonteCarlo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Marek
 */
public abstract class AstractMonteCarlo {

    public abstract void beforeSimulation();

    public abstract AbstractResultBox afterSimulation();

    public AbstractResultBox doReplication(int numberOfReplication) {
        beforeSimulation();
        for (int i = 0; i < numberOfReplication; i++) {
            insideOfReplication();
            //observer
        }
        return afterSimulation();
    }

    public abstract void insideOfReplication();
}
