/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

/**
 *
 * @author Marek
 */
public class ProbabilityException extends Exception {

    public ProbabilityException() {
        super();
    }

    public ProbabilityException(String message) {
        super(message);
    }

    public ProbabilityException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProbabilityException(Throwable cause) {
        super(cause);
    }

}
