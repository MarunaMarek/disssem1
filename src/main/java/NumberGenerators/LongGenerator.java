/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumberGenerators;

import java.util.Random;

/**
 *
 * @author Marek
 */
public class LongGenerator implements InterfaceGenerator<Long> {

    private final Random generator;

    public LongGenerator(long seed) {
        generator = new Random(seed);
    }

    public LongGenerator() {
        generator = new Random(SeedUtility.getSeed());
    }

    @Override
    public Long next() {
        return generator.nextLong();
    }
}
