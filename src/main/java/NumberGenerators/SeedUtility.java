/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumberGenerators;

import java.util.Random;

/**
 *
 * @author Marek
 */
public class SeedUtility {

    private static Random seedGenerator;

    public static long getSeed() {
        return seedGenerator.nextLong();
    }
}
