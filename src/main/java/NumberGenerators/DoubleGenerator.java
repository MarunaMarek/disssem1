/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumberGenerators;

import java.util.Random;

/**
 *
 * @author Marek
 */
public class DoubleGenerator implements InterfaceGenerator<Double>, InterfaceUniformGenerator<Double> {

    private final Random generator;

    public DoubleGenerator(long seed) {
        generator = new Random(seed);
    }

    public DoubleGenerator() {
        generator = new Random(SeedUtility.getSeed());
    }

    @Override
    public Double next() {
        return generator.nextDouble();
    }

    @Override
    public Double next(Double from, Double to) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
