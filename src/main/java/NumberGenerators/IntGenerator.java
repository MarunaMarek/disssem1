/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumberGenerators;

import java.util.Random;

/**
 *
 * @author Marek
 */
public class IntGenerator implements InterfaceGenerator<Integer>, InterfaceUniformGenerator<Integer> {

    private final Random generator;

    public IntGenerator(long seed) {
        generator = new Random(seed);
    }

    public IntGenerator() {
        generator = new Random(SeedUtility.getSeed());
    }

    @Override
    public Integer next() {
        return generator.nextInt();
    }

    public Integer next(int bound) {
        return generator.nextInt(bound);
    }

    @Override
    public Integer next(Integer from, Integer to) {
        return generator.nextInt(); //TODO
    }
}
