/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumberGenerators.ComplexGenerators;

import Exceptions.ProbabilityException;

/**
 *
 * @author Marek
 * @param <T>
 */
public class EmpiricValue<T> implements Comparable<EmpiricValue<T>> {

    private T from;
    private T to;
    private double probability;

    public EmpiricValue(T from, T to, double probability) throws ProbabilityException {
        if (probability < 0.0 || probability > 1.0) {
            throw new ProbabilityException("Probability lower then zero or higher than one");
        }
        this.from = from;
        this.to = to;
        this.probability = probability;
    }

    public T getFrom() {
        return from;
    }

    public T getTo() {
        return to;
    }

    public void setFrom(T from) {
        this.from = from;
    }

    public void setTo(T to) {
        this.to = to;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public int getIntProbability() {
        return (int) probability * 100;
    }

    public void setIntProbability(int probability) {
        this.probability = probability / 100.0;
    }

    @Override
    public int compareTo(EmpiricValue<T> o) {
        if (getProbability() > o.getIntProbability()) {
            return 1;
        }
        if (getProbability() < o.getIntProbability()) {
            return -1;
        }
        return 0;
    }

}
