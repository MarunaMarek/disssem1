/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumberGenerators.ComplexGenerators;

import NumberGenerators.DoubleGenerator;
import NumberGenerators.InterfaceGenerator;

/**
 *
 * @author Marek
 */
public class TriangularGenerator implements InterfaceGenerator<Double> {

    private double min; //a
    private double max; //b
    private double middle; //c
    private DoubleGenerator doubleGenerator;
    private double F; // F(c) middle of cummulative distribution function

    public TriangularGenerator(double a, double b, double c) {
        this.min = a;
        this.max = b;
        this.middle = c;
        doubleGenerator = new DoubleGenerator();
        F = (c - a) / (b - a);
    }

    public TriangularGenerator(double min, double max, double middle, long seed) {
        this.min = min;
        this.max = max;
        this.middle = middle;
        doubleGenerator = new DoubleGenerator(seed);
    }

    @Override
    public Double next() {
        double upperOrLower = doubleGenerator.next();
        if (upperOrLower < F) {
            return (min + Math.sqrt(upperOrLower * (max - min) * (middle - min)));
        } else {
            return (max - Math.sqrt((1 - upperOrLower) * (max - min) * (max - middle)));
        }
    }

}
