/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumberGenerators.ComplexGenerators;

import NumberGenerators.DoubleGenerator;
import NumberGenerators.InterfaceGenerator;
import NumberGenerators.InterfaceUniformGenerator;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Marek
 * @param <T>
 */
public class EmpiricGenerator<T> implements InterfaceGenerator<T> {

    private DoubleGenerator probabilityGenerator;
    private InterfaceUniformGenerator<T> uniformGenerator;
    private ArrayList<EmpiricValue<T>> probabilityList;

    public EmpiricGenerator(ArrayList<EmpiricValue<T>> probabilityList, InterfaceUniformGenerator<T> uniformGenerator) {
        if (probabilityTest(probabilityList)) {
            throw new IllegalStateException("Probability is not 1");
        }
        this.probabilityList = probabilityList;
        this.probabilityGenerator = new DoubleGenerator();
        sortValues();
        this.uniformGenerator = uniformGenerator;
    }

    private T getFromInterval(T from, T to) {
        return uniformGenerator.next(from, to);
    }

    @Override
    public T next() {
        double probability = probabilityGenerator.next();
        double probabilitySum = 0;
        for (EmpiricValue<T> empiricValue : probabilityList) {
            probabilitySum += empiricValue.getProbability();
            if (probability < probabilitySum) {
                return getFromInterval(empiricValue.getFrom(), empiricValue.getTo());
            }
        }
        return null;
    }

    private void sortValues() {
        Collections.sort(probabilityList);
    }

    private boolean probabilityTest(ArrayList<EmpiricValue<T>> probabilityList) {
        double sum = 0;
        for (EmpiricValue empiricValue : probabilityList) {
            sum += empiricValue.getProbability();
        }
        if (sum != 1) {
            return false;
        }
        return true;
    }
}
