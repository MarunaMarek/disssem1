/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumberGenerators.ComplexGenerators;

import NumberGenerators.DoubleGenerator;
import NumberGenerators.InterfaceGenerator;

/**
 *
 * @author Marek
 */
public class ExponentialGenerator implements InterfaceGenerator<Double> {

    private final DoubleGenerator doubleGenerator;
    private final double lambda;

    public ExponentialGenerator(double lambda) {
        this.lambda = lambda;
        doubleGenerator = new DoubleGenerator();
    }

    public ExponentialGenerator(double lambda, long seed) {
        this.lambda = lambda;
        doubleGenerator = new DoubleGenerator(seed);
    }

    @Override
    public Double next() {
        return Math.log(1 - doubleGenerator.next()) / (-lambda);
    }

}
