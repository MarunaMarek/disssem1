/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstactEventSimulation;

/**
 *
 * @author Marek
 */
public interface IExecutable {

    public void execute();

    public double getTime();

    public Simulation getCurrentSimulation();
}
