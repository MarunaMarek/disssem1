/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstactEventSimulation;

/**
 *
 * @author Marek
 */
public abstract class Event implements IExecutable {

    private double eventTime;
    private Simulation currentSimulation;

    @Override
    public abstract void execute();

    @Override
    public double getTime() {
        return eventTime;
    }

    @Override
    public Simulation getCurrentSimulation() {
        return currentSimulation;
    }

}
