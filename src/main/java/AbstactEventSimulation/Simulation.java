/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstactEventSimulation;

import java.util.PriorityQueue;

/**
 *
 * @author Marek
 */
public abstract class Simulation {

    private double currentTime;
    private PriorityQueue<Event> timeline;
    private double endOfTime;

    public void simulate() {
        while (!timeline.isEmpty() || currentTime < endOfTime) {
            Event event = timeline.poll();
            currentTime += event.getTime();
            event.execute();
        }
    }

    public boolean planEvent(Event event, double time) {
        if (time < currentTime) {
            return false;
        } else {
            timeline.add(event);
            return true;
        }
    }
}
