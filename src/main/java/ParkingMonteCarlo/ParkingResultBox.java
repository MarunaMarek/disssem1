package ParkingMonteCarlo;


import AbstractMonteCarlo.AbstractResultBox;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Marek
 */
public class ParkingResultBox extends AbstractResultBox {

    private double strategyOneParameter;
    private double strategyTwoParameter;

    public ParkingResultBox(double parameterOne, double parameterTwo) {
        this.strategyOneParameter = parameterOne;
        this.strategyTwoParameter = parameterTwo;
    }

    public double getStrategyOneParameter() {
        return strategyOneParameter;
    }

    public void setStrategyOneParameter(double strategyOneParameter) {
        this.strategyOneParameter = strategyOneParameter;
    }

    public double getStrategyTwoParameter() {
        return strategyTwoParameter;
    }

    public void setStrategyTwoParameter(double strategyTwoParameter) {
        this.strategyTwoParameter = strategyTwoParameter;
    }

}
