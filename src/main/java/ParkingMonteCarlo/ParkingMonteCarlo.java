/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ParkingMonteCarlo;

import AbstractMonteCarlo.AbstractResultBox;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Marek
 */
public class ParkingMonteCarlo extends AbstractMonteCarlo.AstractMonteCarlo {

    private ArrayList<Random> generatorPool;
    private boolean[] parkingSlots;
    private int numberOfCars;
    private Random seedGenerator;
    private Random cars;
    private int replications;
    private double result1;
    private double result2;
    private int[] results1;
    private int[] results2;

    public ParkingMonteCarlo(int numberOfParkingSlot, long seed) {
        if (seed != 0L) {
            seedGenerator = new Random(seed);
        } else {
            seedGenerator = new Random();
        }
        generatorPool = new ArrayList<>(numberOfParkingSlot);
        parkingSlots = new boolean[numberOfParkingSlot];
        initParkingLots(numberOfParkingSlot);
        cars = new Random(seedGenerator.nextLong());
        initGeneratorPool(numberOfParkingSlot);
        replications = 0;
        result1 = 0;
        result2 = 0;
        results1 = new int[(numberOfParkingSlot * 2) + 1];
        results2 = new int[(numberOfParkingSlot * 2) + 1];
        beforeSimulation();
    }

    private void initParkingLots(int numberOfParkingSlot) {
        for (int i = 0; i < numberOfParkingSlot; i++) {
            parkingSlots[i] = false;
        }
    }

    private void initGeneratorPool(int numberOfParkingSlot) {
        for (int i = 0; i < numberOfParkingSlot; i++) {
            generatorPool.add(new Random(seedGenerator.nextLong()));
        }
    }

    @Override
    public AbstractResultBox doReplication(int numberOfReplication) {
        return super.doReplication(numberOfReplication);
    }

    public int[] getFirstResults() {
        return results1;
    }

    public int[] getSecondResults() {
        return results2;
    }

    private void randomlyFillParkingSlots() {
        int place, counterOfFree, counterOfAll;
        boolean placed;
        for (int i = 0; i < numberOfCars; i++) {
            place = generatorPool.get(i).nextInt(parkingSlots.length - i);
            placed = false;
            counterOfAll = 0;
            counterOfFree = 0;
            while (!placed) {
                if (counterOfFree == place && !parkingSlots[counterOfAll]) {
                    parkingSlots[counterOfAll] = true;
                    placed = true;
                } else {
                    if (parkingSlots[counterOfAll] == true) {
                        counterOfAll++;
                    } else {
                        counterOfAll++;
                        counterOfFree++;
                    }
                }
            }
        }
    }

    private double tryToPark1Srategy() {
        double result = -1;
        int actualPlace = parkingSlots.length - 1;
        boolean parked = false;
        while (!parked && actualPlace >= 0) {
            if (!parkingSlots[actualPlace]) {
                parked = true;
                result = actualPlace;
            } else {
                actualPlace--;
            }
        }
        if (result == -1d) {
            return parkingSlots.length * 2;
        } else {
            return result + 1;
        }
    }

    private double tryToPark2Srategy() {
        double result = -1;
        double temp = (parkingSlots.length) * 2d / 3d;
        int tempPlace = (int) (Math.ceil(temp));
        int actualPlace = parkingSlots.length - 1 - tempPlace;
        boolean parked = false;
        while (!parked && actualPlace >= 0) {
            if (!parkingSlots[actualPlace]) {
                parked = true;
                result = actualPlace;
            } else {
                actualPlace--;
            }
        }
        if (result == -1d) {
            return parkingSlots.length * 2;
        } else {
            return result + 1;
        }
    }

    @Override
    public void beforeSimulation() {

    }

    @Override
    public AbstractResultBox afterSimulation() {
        return new ParkingResultBox(result1 / replications, result2 / replications);
    }

    @Override
    public void insideOfReplication() {
        numberOfCars = 1 + cars.nextInt(parkingSlots.length);
        initParkingLots(parkingSlots.length);
        randomlyFillParkingSlots();
        // first strategy
        double temp1 = tryToPark1Srategy();
        result1 += temp1;
        results1[(int) temp1]++;
        //second strategy
        double temp2 = tryToPark2Srategy();
        result2 += temp2;
        results2[(int) temp2]++;
        replications++;
    }
}
